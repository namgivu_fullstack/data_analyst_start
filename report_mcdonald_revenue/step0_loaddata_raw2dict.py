import importlib
import re
from pathlib import Path
from dateutil import parser

from doc.yearly_quarterly_revenue import yearly_revenue    as yearly_revenue_s
from doc.yearly_quarterly_revenue import quarterly_revenue as quarterly_revenue_s


##region loaddata @ yearly
def load_data_yearly():
    '''
    yearly
    2022	$23,183
    2021	$23,223
    '''

    #region loaddata
    yearly_revenue_l = []

    r_list = yearly_revenue_s.split('\n')
    r_list = [r for r in r_list if r]
    for r in r_list:
        '''
        (.+)\s+\$(.+)
        2022	$23,183
        '''
        m_all = re.findall('(.+)\s+(\$)(.+)', r)[0]  #TODO make $ more general
        year    =   int(m_all[0])
        unit    =       m_all[1]
        revenue = float(m_all[2].replace(',',''))

        d = {
            'year'    : year,
            'revenue' : revenue,
            'unit'    : unit,
        }
        yearly_revenue_l.append(d)
    #endregion loaddata

    #region write to file
    SH = Path(__file__).parent
    f = open(f'{SH}/zo_yearly_revenue.py', 'w')

    f.write(f'yearly_revenue_l={yearly_revenue_l}')  #TODO reader-friendly format data

    f.write('\n') ; f.close()
    #endregion write to file
load_data_yearly()
##endregion loaddata @ yearly


##region loaddata @ quarterly
def load_data_quarterly():
    '''
    quarterly
    2023-03-31	$5,898
    2022-12-31	$5,927
    '''

    #region loaddata
    quarterly_revenue_l = []

    r_list = quarterly_revenue_s.split('\n')
    r_list = [r for r in r_list if r]
    for r in r_list:
        '''
        (.+)      \s+\$(.+)
        2022-12-31	  $5,927
        '''
        m_all = re.findall('(.+)\s+(\$)(.+)', r)[0]  #TODO make $ more general
        quarter_date   = parser.parse(m_all[0])  # for sorting
        quarter_date_s =              m_all[0]   # for output
        unit           =              m_all[1]
        revenue        =        float(m_all[2].replace(',',''))

        d = {
            'quarter_date'   : quarter_date,
            'quarter_date_s' : quarter_date_s,
            'revenue'        : revenue,
            'unit'           : unit,
        }
        quarterly_revenue_l.append(d)
    #endregion loaddata

    #region write to file
    SH = Path(__file__).parent
    f = open(f'{SH}/zo_quarterly_revenue.py', 'w')

    quarterly_revenue_l__nodateobj = []
    for d_ in quarterly_revenue_l:
        d = {** d_} ; del d['quarter_date']
        quarterly_revenue_l__nodateobj.append(d)
    f.write(f'quarterly_revenue_l={quarterly_revenue_l__nodateobj}')  #TODO reader-friendly format data

    f.write('\n') ; f.close()
    #endregion write to file
load_data_quarterly()
##endregion loaddata @ quarterly

i=122

from pathlib import Path

from report_mcdonald_revenue.zo_yearly_revenue    import yearly_revenue_l
from report_mcdonald_revenue.zo_quarterly_revenue import quarterly_revenue_l


def make_yearly_report():
    def compare_yearly_d(e):
        return e['revenue']

    yearly_revenue_sorted = sorted(yearly_revenue_l, key=compare_yearly_d, reverse=True)
    report_row_all = []
    for d in yearly_revenue_sorted:
        report_row = f'''{d['year']} {d['revenue']}'''
        report_row_all.append(report_row)

    #region write to file
    SH = Path(__file__).parent
    f = open(f'{SH}/zreport_yearly.txt', 'w')

    nl = '\n'

    f.write(f'''
--- best-performance year
{report_row_all[0]}

--- all years
{nl.join(report_row_all)}
    '''.strip()+'\n\n')

    f.write('\n') ; f.close()
    #endregion write to file
make_yearly_report()


def make_quarterly_report():
    def compare_quarterly_d(e):
        return e['revenue']

    quarterly_revenue_sorted = sorted(quarterly_revenue_l, key=compare_quarterly_d, reverse=True)
    report_row_all = []
    for d in quarterly_revenue_sorted:
        report_row = f'''{d['quarter_date_s']} {d['revenue']}'''
        report_row_all.append(report_row)

    #region write to file
    SH = Path(__file__).parent
    f = open(f'{SH}/zreport_quarterly.txt', 'w')

    nl = '\n'

    f.write(f'''
--- best-performance quarter
{report_row_all[0]}

--- all quarters
{nl.join(report_row_all)}
    '''.strip() + '\n\n')

    f.write('\n') ; f.close()
    #endregion write to file
make_quarterly_report()
